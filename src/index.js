(function(window) {
  const { document } = window;
  const id = 'yt-pause-on-blur';

  // Initialize
  if (!document.getElementById(id)) {
    window.addEventListener('blur', onBlur);
    window.addEventListener('focus', onFocus);

    const indicator = document.createElement('div');
    indicator.id = id;
    indicator.style = 'position: fixed; bottom: .5rem; left: .5rem; padding: 1rem; border: 1px solid #fff; border-radius: 8px; background-color: #000; color: #fff; font-size: 16px;';
    indicator.innerHTML = 'yt-pause-on-blur<button style="margin-left: 1rem;">Off</button>';
    document.body.appendChild(indicator);

    offButton().addEventListener('click', () => {
      window.removeEventListener('blur', onBlur);
      window.removeEventListener('focus', onFocus);

      document.body.removeChild(indicator);
    });
  }

  function onBlur() {
    if (!isPaused()) {
      toggle();
    }
  }

  function onFocus() {
    if (isPaused()) {
      toggle();
    }
  }

  function isPaused() {
    return videoPlayer().paused;
  }

  function toggle() {
    pauseButton().click();
  }

  function videoPlayer() {
    return getElement('#movie_player video');
  }

  function pauseButton() {
    return getElement('[aria-keyshortcuts="k"]');
  }

  function offButton() {
    return getElement(`#${id} > button`);
  }

  function getElement(selector) {
    const el = document.querySelector(selector);
    if (!el) throw new Error(`Cannot find: ${selector}`);
    return el;
  }
}(window));
