const fs = require('node:fs/promises');
const { minify } = require('terser');

module.exports = function() {
  return fs.readFile('./src/index.js')
    .then(buffer => processJs(buffer.toString('utf-8')));
};

function processJs(js) {
  return minify(js, {
    compress: {
      negate_iife: false
    }
  })
    .then(result => result.code)
    .then(code => {
      console.log(code.length);
      console.log(code);
      return code;
    })
}
